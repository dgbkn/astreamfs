/* Alain BENEDETTI
 * This is a very simple program (derived from curl's example/simple.c)
 * to measure the impact of (a single!) memcpy on efficiency.
 * 
 * The program does the same thing as simple.c: do a HTTP GET and display
 * the result on the terminal with some modifications:
 * - accepts the URL to "GET" as parameter
 * - some pedantic things: global init/cleanup
 * - has a callback so that we can test with a memcpy added or not.
 * 
 * Please note that this memcpy is even in a favourable situation since it
 * can benefit from processor caching: it is done right when the curl's
 * buffer is received, and could be cached for write() that is done with no
 * other instructions in the between.
 * In other situations where there might be context switches or the like,
 * memcpy would less benefit from caching.
 * 
 * Instructions:
 * 
 * Compile with: 
 *	cc -Wall -O3 benchmark.c        -o benchmark1 $(curl-config --cflags --libs)
 *	cc -Wall -O3 benchmark.c -DSLOW -o benchmark2 $(curl-config --cflags --libs)
 * 
 * Benchmark:
 * 
 * 	I have chosen the test file: http://speedtest4.1fichier.com/p1.dat
 * 	because:
 *	-1) it is unencrypted (no TLS) thus the percentage will be showing more
 *	-2) size is 10GB (enough to be significant
 *	-3) test with ethernet + fibre at 1GBps
 *	-4) it is a "performance test" file, through put is steady around 1Gbps
 *
 * 	Adapt to a local provider file where you will get the equivalent.
 * 
 * 
 * 	On a linux machine, run:
 * for i in $(seq 1 5); do /usr/bin/time -f "User time (seconds): %U" ./benchmark1 http://speedtest4.1fichier.com/p1.dat >/dev/null; done
 * 
 * and
 * 
 * for i in $(seq 1 5); do /usr/bin/time -f "User time (seconds): %U" ./benchmark2 http://speedtest4.1fichier.com/p1.dat >/dev/null; done
 * 
 * 	This should produce 5 timings of how long you spent in "userland" for
 *	each case: without and with memcpy, that instruction being the only
 *	difference between both programs.
 *
 * 
 * My results:
--------------------------
Intel(R) Core(TM) i7-6700 CPU @ 3.40GHz / 64bits O.S.
 
No memcpy: average=12.44
User time (seconds): 12.14
User time (seconds): 12.32
User time (seconds): 12.77
User time (seconds): 12.32
User time (seconds): 12.64

With memcpy: average=13.81 (+11% impact)
User time (seconds): 13.52
User time (seconds): 14.11
User time (seconds): 13.75
User time (seconds): 14.07
User time (seconds): 13.58

--------------------------
Raspberry Pi 4 Model B Rev 1.1 [ARMv7 Processor rev 3 (v7l)] / 32bits O.S.

No memcpy: average=10.10
User time (seconds): 9.96
User time (seconds): 10.44
User time (seconds): 10.14
User time (seconds): 10.00
User time (seconds): 9.94

With memcpy: average=11.65 (+15% impact)
User time (seconds): 11.36
User time (seconds): 11.82
User time (seconds): 11.45
User time (seconds): 11.57
User time (seconds): 12.05

--------------------------
Question: measure "user time" or "elapsed"?

  "elapsed" is what the user sees, so it makes sense, but...
    Since it combines all times: network + kernel + libcurl/callback, and
    due to how kernels work, in many situation it will not show the "cost" of
    a modification that was done in libcurl/callback.
    In fact, when the userland code libcurl/callback is running, the data will
    continue to arrive via the socket and be placed in kernel buffers, in a
    way that can be thought "in parallel" (although it might happen serially
    via interrupts).
    
    Representation (X is the added overhead we want to measure)
 User : [-----X]              [-----X]          [-----X]              [-----X]
 K+Net: ____[--]____[--]____[--]____[--]____[--]____[--]____[--]____[--]____
    
    In this case, the data is computed fast enough in userspace so that it
    has to wait for data to arrive in the buffer. That means that "elapsed"
    will be mostly unaffected by our overhead.

    Note that in fact it might be "pseudo-parellelism" in the same thread (or
    if your machine has only one core!), like that:
 User : [---\  /--X]           /[---\  /--X]   /[---\  /--X]           /[-----X]
 K+Net: ____[--]____[--]____[--]____[--]____[--]____[--]____[--]____[--]____

    The only difference between both graphs is "when" the overhead will show.
    In the first one it is when "user" is slower than the network, on the
    second one "user+kernel" must be slower.
    But as long as that is true, any overhead added won't show.
    
    Conversly, if the time used by user (or user+kernel) is higher than the
    network time, any delay added on the callback or library will show in
    "elapsed".

   Conclusion: "elapsed" is good because it is how things "feel like" for the
     user, but does not always show how efficient (or non-efficient) is the
     code added in libcurl/callback.
     "elapsed" is the right measure when the impact is visible since it adds
     both impacts on user and kernel.
     But since that overhead might not show and affect the user in other
     situations (faster network, slower CPU, high impact operations like crypto
     -TLS-,...), measuring "elapsed" is not always a good way to predict how
     badly the overhead can affect the user, and in that case "user" is
     a better approximation.

     In the above measures in plain HTTP, both CPU can sustain 1Gbps and
     being faster, the overhead completely disappear from "elapsed" but
     shows in "user".

     To illustrate a "normal" situation where the overhead affects the user
     the same test is ran with TLS (https) on the Raspberry Pi 4 Model B.
     
     The code was also changed with 10 memcpy instead of 1, so that the
     impact on the end user is clear.
     
Command for the benchmark:
- Same as above, just change in the URL with 'https' instead of 'http'.

Methodology:
- Commands where ran 10 times and the 2 times that where diverging the most
  from the average "elapsed" where removed. Indeed if the server slows down
  or the machine running the test uses some network (background apt update!)
  the "elapsed" can be off (although the user/kernel do not vary too much).

--------------------------------------
Results (Raspberry Pi 4 Model B - test with TLS)
(Fast)
     User     Kernel  Elapsed
     190,64   50,55   04:03,64
     192,65   59,86   04:15,13
     190,06   53,18   04:05,21
     192,21   48,96   04:03,53
     191,39   52,48   04:06,74
     190,67   48,21   04:01,31
     190,53   48,78   04:01,52
     190,92   48,23   04:01,34
     ------   -----   --------
Avg  191,134  51,281  04:04,80

(Slow)
     User     Kernel  Elapsed
     220,71   53,90   04:37,12
     215,86   52,55   04:30,80
     219,60   51,63   04:33,75
     219,58   56,42   04:38,77
     218,11   53,35   04:33,97
     218,79   53,00   04:34,76
     217,69   53,70   04:33,86
     217,64   49,07   04:29,64
    ------   -----   --------
Avg  218,498  52,953  04:34,08

Over  27,364   1,671	 29,28

===================

Global summary:
- Impact of a memcpy: ~2,9 sec (2,74 U + 0,16 K)
- "User" gives a good approximation even when it does not show on "elapsed".

(Note 3 sec could be over-estimated since 10 copies trigger more cache eviction)
 
 */ 
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <curl/curl.h>

static size_t out(const char *ptr, const size_t size, const size_t nmemb, 
	          void *unused_userdata)
{
	(void)unused_userdata;
#ifdef SLOW	
	char buf[10 * size * nmemb];
	int i;


	for (i=0; i<10; i++)
		memcpy(buf + i * size * nmemb, ptr, size * nmemb);
#else
	#define buf ptr
#endif 
	return write(1, buf, size * nmemb);
}


int main(int argc, char *argv[])
{
	CURL *curl;
	CURLcode res;

	if (argc < 2) {
		fprintf(stderr, "Usage: %s URL\n", argv[0]);
		return 1;
	}

	res = curl_global_init(CURL_GLOBAL_DEFAULT);
	if (res != CURLE_OK) {
		fprintf(stderr, "curl_global_init() failed: %s\n",
			curl_easy_strerror(res));
		return 1;
	}
	curl = curl_easy_init();
	if(curl != NULL) {
		curl_easy_setopt(curl, CURLOPT_URL, argv[1]);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, out);
 
		res = curl_easy_perform(curl);
		if(res != CURLE_OK)
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
				curl_easy_strerror(res));
		curl_easy_cleanup(curl);
	}
	else {
		fprintf(stderr, "Error opening: %s\n", argv[1]);
	}

	curl_global_cleanup();
	return 0;
}
